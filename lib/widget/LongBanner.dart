import 'package:flutter/material.dart';

class LongBanner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      width: 130,
      height: 258,
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      child: Image.asset(
        'images/gambar(6).jpg',
        fit: BoxFit.cover,
      ),
    );
  }
}
