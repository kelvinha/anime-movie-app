import 'package:flutter/material.dart';

class SmallBanner extends StatelessWidget {
  final String alamat;

  SmallBanner({this.alamat});

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      width: 110,
      height: 240 / 2,
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      child: Image.asset(
        'images/' + alamat,
        fit: BoxFit.cover,
      ),
    );
  }
}
