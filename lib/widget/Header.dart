import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Icon(
          Icons.menu,
          size: 36,
        ),
        CircleAvatar(
          radius: 25,
          foregroundImage: NetworkImage(
              'https://img2.apksum.com/af/com.hussamaldean.jujujaisen/3/icon.png'),
        ),
      ],
    );
  }
}
