import 'package:flutter/material.dart';

class FormSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      child: TextFormField(
        cursorColor: Colors.black,
        style: TextStyle(fontSize: 18),
        decoration: InputDecoration(
          hintText: 'Search Here ..',
          prefixIcon: Icon(
            Icons.search,
            color: Colors.black,
          ),
          contentPadding: EdgeInsets.fromLTRB(12, 2, 12, 0),
          border: InputBorder.none,
        ),
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: Colors.black),
      ),
    );
  }
}
