import 'package:flutter/material.dart';
import 'package:latihan_ui_layout/widget/FormSearch.dart';
import 'package:latihan_ui_layout/widget/Header.dart';
import 'package:latihan_ui_layout/widget/LongBanner.dart';
import 'package:latihan_ui_layout/widget/SmallBanner.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF3F4F5),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Header(),
                SizedBox(
                  height: 15,
                ),
                FormSearch(),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Top 5 Animes',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Text(
                      'View more idols',
                      style: TextStyle(
                        color: Colors.lightBlue,
                        decoration: TextDecoration.underline,
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    LongBanner(),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        SmallBanner(
                          alamat: 'gambar(5).jpg',
                        ),
                        SizedBox(
                          height: 18,
                        ),
                        SmallBanner(
                          alamat: 'gambar(7).jpg',
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Column(
                      children: [
                        SmallBanner(
                          alamat: 'gambar(8).jpg',
                        ),
                        SizedBox(
                          height: 18,
                        ),
                        SmallBanner(
                          alamat: 'gambar(9).jpg',
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Recomended Anime',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Text(
                      'View more idols',
                      style: TextStyle(
                        color: Colors.lightBlue,
                        decoration: TextDecoration.underline,
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      SmallBannerList(
                        alamat: 'gambar(6).jpg',
                      ),
                      SmallBannerList(
                        alamat: 'gambar(7).jpg',
                      ),
                      SmallBannerList(
                        alamat: 'gambar(8).jpg',
                      ),
                      SmallBannerList(
                        alamat: 'gambar(9).jpg',
                      ),
                      SmallBannerList(
                        alamat: 'gambar(5).jpg',
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SmallBannerList extends StatelessWidget {
  final String alamat;

  SmallBannerList({this.alamat});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 10),
      clipBehavior: Clip.antiAlias,
      width: 133,
      height: 110,
      decoration: BoxDecoration(
        color: Colors.black26,
        borderRadius: BorderRadius.all(
          Radius.circular(20),
        ),
      ),
      child: Image.asset(
        'images/' + alamat,
        fit: BoxFit.cover,
      ),
    );
  }
}
